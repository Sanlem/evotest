import os

DEBUG = False

PORT = 80
HOST = '0.0.0.0'

# application directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# db
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'app.db')
DATABASE_CONNECT_OPTIONS = {}

# Application threads
THREADS_PER_PAGE = 2

# Enable protection agains *Cross-site Request Forgery (CSRF)*
WTF_CSRF_ENABLED = True
SECRET_KEY = "dasdsad.,m,m,m1w"

# available languages
LANGUAGES = {
    'en': 'English',
    'ua': 'Ukrainian'
}
