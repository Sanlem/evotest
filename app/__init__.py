from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_babel import Babel

# init app
app = Flask(__name__)

# loading configs
app.config.from_object('config')

# init db object
db = SQLAlchemy(app)

# init migrations plugin
migrate = Migrate(app, db)

# init Babel
babel = Babel(app)

# register blueprints
from app.departments import departments
from app.typical_positions import typical_positions
# blueprint is imported from views to avoid circular imports
from app.vacancies.views import vacancies
from app.employees import employees
from app.positions import positions

app.register_blueprint(vacancies, url_prefix='/vacancies')
app.register_blueprint(departments, url_prefix='/departments')
app.register_blueprint(typical_positions, url_prefix='/typical_positions')
app.register_blueprint(employees, url_prefix='/employees')
app.register_blueprint(positions, url_prefix='/positions')


@app.route("/")
def index():
    return render_template('index.html')


@app.errorhandler(404)
def not_found(e):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_server_error(e):
    return render_template('500.html'), 500

# import models, so Alembic would see them
from app.departments.models import *
from app.typical_positions.models import *
from app.vacancies.models import *
