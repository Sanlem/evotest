from flask import Blueprint, url_for
from flask_babel import gettext as _
from app.base.generic_views import CreateView, EditView, DetailView, ListView
from .forms import VacancyForm
from .models import Vacancy

vacancies = Blueprint('vacancies', __name__)


class VacancyListView(ListView):
    template_name = 'vacancies/list.html'

    def get_queryset(self):
        return Vacancy.query.all()


class VacancyDetailView(DetailView):
    model = Vacancy
    template_name = 'vacancies/detail.html'


class VacancyCreateView(CreateView):
    model = Vacancy
    form_class = VacancyForm
    template_name = 'vacancies/create.html'
    success_flash_message = _('Successfully created new vacancy.')

    def get_success_redirect_url(self):
        return url_for('vacancies.list')


class VacancyEditView(EditView):
    model = Vacancy
    form_class = VacancyForm
    template_name = 'vacancies/edit.html'
    success_flash_message = _('Successfully edited.')

    def get_success_redirect_url(self):
        return url_for('vacancies.detail',
                       pk=self.object.id)


vacancies.add_url_rule('/', view_func=VacancyListView.as_view('list'))

vacancies.add_url_rule('/<int:pk>',
                       view_func=VacancyDetailView.as_view('detail'))

vacancies.add_url_rule('/create',
                       view_func=VacancyCreateView.as_view('create'))

vacancies.add_url_rule('/<int:pk>/edit',
                       view_func=VacancyEditView.as_view('edit'))
