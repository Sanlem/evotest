from datetime import date
from flask_babel import gettext as _
from app import db
from app.departments.models import Department
from app.typical_positions.models import TypicalPosition


class Vacancy(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date_opened = db.Column(db.Date, info={'label': _('Date opened')},
                            nullable=True, default=date.today())
    date_closed = db.Column(db.Date, nullable=True,
                            info={'label': _('Date closed')})
    department_id = db.Column(db.Integer, db.ForeignKey('department.id'),
                              nullable=False)
    department = db.relationship(Department, backref='vacancies')
    typical_position_id = db.Column(db.Integer, db.ForeignKey('typical_position.id'),
                                    nullable=False)
    typical_position = db.relationship(TypicalPosition, backref='vacancies')
    is_closed = db.Column(db.Boolean, default=False,
                          nullable=False)
