from wtforms import SelectField
from flask_babel import gettext as _
from app.base.forms import ModelForm
from app.departments.models import Department
from app.typical_positions.models import TypicalPosition
from .models import Vacancy


class VacancyForm(ModelForm):
    class Meta:
        model = Vacancy
        exclude = ['is_closed']

    department_id = SelectField(_('Department'), coerce=int)
    typical_position_id = SelectField(_('Typical position'), coerce=int)

    def __init__(self, *args, **kwargs):
        super(VacancyForm, self).__init__(*args, **kwargs)
        # recalculate choices on each new request
        self.department_id.choices = [(d.id, d.name) for d in Department.query.all()]
        self.typical_position_id.choices = [(d.id, d.name) for d in TypicalPosition.query.all()]
