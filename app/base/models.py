from flask_babel import gettext as _
from app import db


class BaseOrgStructureModel(db.Model):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True, nullable=False,
                     info={'label': _('Name')})
    description = db.Column(db.Text(500), nullable=True,
                            info={'label': _('Description')})
