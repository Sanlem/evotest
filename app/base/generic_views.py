from flask import redirect, flash, render_template, request
from flask.views import MethodView
from app import db


class BaseGenericView(MethodView):
    model = None
    template_name = None

    def get_object(self, pk=None):
        if pk is not None:
            return self.model.query.get_or_404(pk)
        else:
            return self.model()

    def get_context_data(self):
        return {}


class BaseGenericEditView(BaseGenericView):
    success_flash_message = None
    form_class = None

    def pre_commit(self, obj, form):
        return obj

    def post_commit(self, obj, form):
        return

    def save_if_valid_or_error(self, obj, action=None):
        if action not in ['create', 'update']:
            raise ValueError("'action' should be 'create' or 'update'")
        form = self.form_class(request.form, obj=obj)
        if form.validate():
            form.populate_obj(obj)
            obj = self.pre_commit(obj, form)
            if action == 'create':
                db.session.add(obj)
            db.session.commit()
            self.post_commit(obj, form)
            flash(self.success_flash_message)
            return redirect(self.get_success_redirect_url())
        else:
            return render_template(self.template_name,
                                   object=obj,
                                   form=form)

    def get_success_redirect_url(self):
        # this is a separate method because Flask needs
        # context to generate url
        raise NotImplementedError


class CreateView(BaseGenericEditView):
    def get(self):
        obj = self.get_object()
        form = self.form_class(obj=obj)
        context = self.get_context_data()
        return render_template(self.template_name,
                               form=form,
                               **context)

    def post(self):
        obj = self.get_object()
        return self.save_if_valid_or_error(obj, action='create')


class EditView(BaseGenericEditView):
    def get(self, pk):
        obj = self.get_object(pk)
        self.object = obj
        form = self.form_class(obj=obj)
        context = self.get_context_data()
        return render_template(self.template_name,
                               form=form,
                               object=obj,
                               **context)

    def post(self, pk):
        obj = self.get_object(pk)
        self.object = obj
        return self.save_if_valid_or_error(obj, action='update')


class DeleteView(BaseGenericEditView):
    prompt_message = None

    def get(self, pk):
        obj = self.get_object(pk)
        self.object = obj
        form = self.form_class(obj=obj)
        context = self.get_context_data()
        return render_template(self.template_name,
                               form=form,
                               object=obj,
                               **context)

    def post(self, pk):
        obj = self.get_object(pk)
        db.session.delete(obj)
        flash(self.success_flash_message)
        return redirect(self.get_success_redirect_url())


class DetailView(BaseGenericView):
    def get(self, pk):
        obj = self.get_object(pk)
        self.object = obj
        context = self.get_context_data()
        return render_template(self.template_name,
                               object=obj,
                               **context)


class ListView(BaseGenericView):
    def get_queryset(self):
        raise NotImplementedError

    def get(self):
        context = self.get_context_data()
        return render_template(self.template_name,
                               objects=self.get_queryset(),
                               **context)
