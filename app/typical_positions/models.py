from app.base.models import BaseOrgStructureModel


class TypicalPosition(BaseOrgStructureModel):
    def __str__(self):
        return 'TypicalPosition({})'.format(self.name)
