from flask import Blueprint, url_for
from flask_babel import gettext as _

from app.base.generic_views import CreateView, EditView, DetailView, ListView
from .forms import TypicalPositionForm
from .models import TypicalPosition

typical_positions = Blueprint('typical_positions', __name__)


class TypicalPositionListView(ListView):
    template_name = 'typical_positions/list.html'

    def get_queryset(self):
        return TypicalPosition.query.all()


class TypicalPositionDetailView(DetailView):
    model = TypicalPosition
    template_name = 'typical_positions/detail.html'


class TypicalPositionCreateView(CreateView):
    model = TypicalPosition
    form_class = TypicalPositionForm
    template_name = 'typical_positions/create.html'
    success_flash_message = _('Successfully created new typical position.')

    def get_success_redirect_url(self):
        return url_for('typical_positions.list')


class TypicalPositionEditView(EditView):
    model = TypicalPosition
    form_class = TypicalPositionForm
    template_name = 'typical_positions/edit.html'
    success_flash_message = _('Successfully edited.')

    def get_success_redirect_url(self):
        return url_for('typical_positions.detail',
                       pk=self.object.id)


typical_positions.add_url_rule('/', view_func=TypicalPositionListView.as_view('list'))

typical_positions.add_url_rule('/<int:pk>',
                               view_func=TypicalPositionDetailView.as_view('detail'))

typical_positions.add_url_rule('/create',
                               view_func=TypicalPositionCreateView.as_view('create'))

typical_positions.add_url_rule('/<int:pk>/edit',
                               view_func=TypicalPositionEditView.as_view('edit'))
