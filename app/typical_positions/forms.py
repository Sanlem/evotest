from app.base.forms import ModelForm
from .models import TypicalPosition


class TypicalPositionForm(ModelForm):
    class Meta:
        model = TypicalPosition
