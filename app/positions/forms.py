from wtforms import SelectField
from flask_babel import gettext as _
from app.base.forms import ModelForm
from app.departments.models import Department
from app.typical_positions.models import TypicalPosition
from app.vacancies.models import Vacancy
from app.employees.models import Employee
from .models import Position


class PositionForm(ModelForm):
    class Meta:
        model = Position

    typical_position_id = SelectField(_('Typical position'), coerce=int)
    vacancy_id = SelectField(_('Vacancy'), coerce=int)
    employee_id = SelectField(_('Employee'), coerce=int)

    def __init__(self, *args, **kwargs):
        super(PositionForm, self).__init__(*args, **kwargs)
        # recalculate choices on each new request
        self.typical_position_id.choices = [(d.id, d.name) for d in TypicalPosition.query.all()]
        self.vacancy_id.choices = [(d.id, '{} - {}'.format(d.department.name, d.typical_position.name))
                                   for d in Vacancy.query.filter_by(is_closed=False).all()]
        self.employee_id.choices = [(d.id, '{} {}'.format(d.name, d.surname)) for d in Employee.query.all()]


class PositionEditForm(PositionForm):
    def __init__(self, *args, **kwargs):
        super(PositionEditForm, self).__init__(*args, **kwargs)
        self.vacancy_id.choices = [(d.id, '{} - {}'.format(d.department.name, d.typical_position.name))
                                   for d in Vacancy.query.all()]
        del self.is_head
