from datetime import date
from flask import Blueprint, url_for
from flask_babel import gettext as _
from app.base.generic_views import CreateView, EditView, DetailView, ListView
from app.vacancies.models import Vacancy, db
from .forms import PositionForm, PositionEditForm
from .models import Position

positions = Blueprint('positions', __name__)


class PositionListView(ListView):
    template_name = 'positions/list.html'

    def get_queryset(self):
        return Position.query.all()


class PositionDetailView(DetailView):
    model = Position
    template_name = 'positions/detail.html'


class PositionCreateView(CreateView):
    model = Position
    form_class = PositionForm
    template_name = 'positions/create.html'
    success_flash_message = _('Successfully created new position.')

    def pre_commit(self, obj, form):
        # Populate department_id fron vacancy and add it
        # to position.
        department_id = Vacancy.query.get(form.data['vacancy_id']).department_id
        obj.department_id = department_id
        return obj

    def post_commit(self, obj, form):
        # close vacancy
        vacancy = Vacancy.query.get(form.data['vacancy_id'])
        vacancy.is_closed = True
        # set leave_date, if employee has positions
        employee_id = form.data['employee_id']
        previous_positions = Position.query.filter_by(employee_id=employee_id).order_by(Position.id.desc()).all()
        if len(previous_positions) > 1:
            # the first one was just created
            today = date.today()
            for v in previous_positions[1:]:
                v.leave_date = today

        db.session.commit()

    def get_success_redirect_url(self):
        return url_for('positions.list')


class PositionEditView(EditView):
    model = Position
    form_class = PositionEditForm
    template_name = 'positions/edit.html'
    success_flash_message = _('Successfully edited.')

    def get_success_redirect_url(self):
        return url_for('positions.detail',
                       pk=self.object.id)


positions.add_url_rule('/', view_func=PositionListView.as_view('list'))

positions.add_url_rule('/<int:pk>',
                       view_func=PositionDetailView.as_view('detail'))

positions.add_url_rule('/create',
                       view_func=PositionCreateView.as_view('create'))

positions.add_url_rule('/<int:pk>/edit',
                       view_func=PositionEditView.as_view('edit'))
