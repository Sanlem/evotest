from datetime import date
from flask_babel import gettext as _
from app import db
from app.departments.models import Department
from app.vacancies.models import Vacancy
from app.employees.models import Employee
from app.typical_positions.models import TypicalPosition


class Position(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    entry_date = db.Column(db.Date, info={'label': _('Entry date')},
                           nullable=True, default=date.today())
    leave_date = db.Column(db.Date, nullable=True,
                           info={'label': _('Leave date')})
    department_id = db.Column(db.Integer, db.ForeignKey('department.id'),
                              nullable=False)
    department = db.relationship(Department, backref='positions')
    typical_position_id = db.Column(db.Integer, db.ForeignKey('typical_position.id'),
                                    nullable=False)
    typical_position = db.relationship(TypicalPosition, backref='positions')
    vacancy_id = db.Column(db.Integer, db.ForeignKey('vacancy.id'))
    vacancy = db.relationship(Vacancy, backref=db.backref('position', uselist=False))
    is_head = db.Column(db.Boolean, default=False,
                        nullable=False,
                        info={'label': _('Is head')})
    employee_id = db.Column(db.Integer, db.ForeignKey('employee.id'))
    employee = db.relationship(Employee, backref='positions')
