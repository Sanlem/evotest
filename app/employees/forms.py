from wtforms import validators
from wtforms.fields.html5 import EmailField
from wtforms.fields.core import StringField
from flask_babel import gettext as _
from app.base.forms import ModelForm
from .models import Employee


class EmployeeForm(ModelForm):
    class Meta:
        model = Employee

    email = EmailField(_('Email'), [validators.DataRequired(), validators.Email()])
    phone = StringField(_('Phone'), [validators.DataRequired(),
                                     validators.Regexp('^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$',
                                                       message=_('Invalid phone number'))])
