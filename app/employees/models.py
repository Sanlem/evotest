import re
from flask_babel import gettext as _
from app import db


class Employee(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False,
                     info={'label': _('Name')})
    surname = db.Column(db.String(20), nullable=False,
                        info={'label': _('Surname')})
    email = db.Column(db.String(30),
                      info={'label': _('Email')})
    phone = db.Column(db.String(20),
                      info={'label': _('Phone')})
    birth_date = db.Column(db.Date, nullable=True,
                           info={'label': _('Birth date')})

    @db.validates('email')
    def validate_email(self, key, address):
        if not re.match(r'[^@]+@[^@]+\.[^@]+', address):
            raise ValueError(_('Invalid email'))
        return address

    @db.validates('phone_number')
    def validate_phone(self, key, phone):
        if not re.match(r'^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$', phone):
            raise ValueError(_('Invalid phone number'))
        return phone
