from datetime import date
from flask import Blueprint, url_for, redirect, flash
from flask_babel import gettext as _
from app.base.generic_views import ListView, DetailView, CreateView, EditView
from app import db
from .models import Employee
from .forms import EmployeeForm

employees = Blueprint('employees', __name__)


class EmployeeListView(ListView):
    template_name = 'employees/list.html'

    def get_queryset(self):
        return Employee.query.all()


class EmployeeDetailView(DetailView):
    model = Employee
    template_name = 'employees/detail.html'


class EmployeeCreateView(CreateView):
    model = Employee
    form_class = EmployeeForm
    template_name = 'employees/create.html'
    success_flash_message = _('Successfully created new employee.')

    def get_success_redirect_url(self):
        return url_for('employees.list')


class EmployeeEditView(EditView):
    model = Employee
    form_class = EmployeeForm
    template_name = 'employees/edit.html'
    success_flash_message = _('Successfully edited.')

    def get_success_redirect_url(self):
        return url_for('employees.detail',
                       pk=self.object.id)


@employees.route('/<int:pk>/fire', methods=['POST'])
def fire(pk):
    # close all employees positions
    employee = Employee.query.get_or_404(pk)
    today = date.today()
    for position in employee.positions:
        if position.leave_date is None:
            position.leave_date = today

    db.session.commit()
    flash(_('Successfully fired an employee.'))
    return redirect(url_for('employees.detail', pk=employee.id))


employees.add_url_rule('/', view_func=EmployeeListView.as_view('list'))

employees.add_url_rule('/<int:pk>',
                       view_func=EmployeeDetailView.as_view('detail'))

employees.add_url_rule('/create',
                       view_func=EmployeeCreateView.as_view('create'))

employees.add_url_rule('/<int:pk>/edit',
                       view_func=EmployeeEditView.as_view('edit'))
