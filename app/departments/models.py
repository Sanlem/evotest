from app.base.models import BaseOrgStructureModel


class Department(BaseOrgStructureModel):
    def __str__(self):
        return 'Department({})'.format(self.name)
