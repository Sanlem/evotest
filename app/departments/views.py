from flask import Blueprint, url_for
from flask_babel import gettext as _

from app.base.generic_views import CreateView, EditView, DetailView, ListView
from .forms import DepartmentForm
from .models import Department

departments = Blueprint('departments', __name__)


class DepartmentListView(ListView):
    template_name = 'departments/list.html'

    def get_queryset(self):
        return Department.query.all()


class DepartmentDetailView(DetailView):
    model = Department
    template_name = 'departments/detail.html'


class DepartmentCreateView(CreateView):
    model = Department
    form_class = DepartmentForm
    template_name = 'departments/create.html'
    success_flash_message = _('Successfully created new department.')

    def get_success_redirect_url(self):
        return url_for('departments.list')


class DepartmentEditView(EditView):
    model = Department
    form_class = DepartmentForm
    template_name = 'departments/edit.html'
    success_flash_message = _('Successfully edited.')

    def get_success_redirect_url(self):
        return url_for('departments.detail', pk=self.object.id)


departments.add_url_rule('/',
                         view_func=DepartmentListView.as_view('list'))

departments.add_url_rule('/<int:pk>',
                         view_func=DepartmentDetailView.as_view('detail'))

departments.add_url_rule('/create',
                         view_func=DepartmentCreateView.as_view('create'))

departments.add_url_rule('/<int:pk>/edit',
                         view_func=DepartmentEditView.as_view('edit'))
