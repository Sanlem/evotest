/**
 * Created by sanlem on 27.03.17.
 */
$(document).ready(function () {
    var $table = $('#positions-table').DataTable({
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        aoColumnDefs: [
            { "bSearchable": false, "bVisible": true, "aTargets": [ 6 ] }
        ],
        buttons: [
            {
                extend: 'excel',
                title: 'Positions',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5]
                }
            },
            {
                extend: 'csv',
                title: 'Positions',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5]
                }
            }
        ]
    });
});
