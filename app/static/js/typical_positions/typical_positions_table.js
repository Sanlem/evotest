/**
 * Created by sanlem on 27.03.17.
 */
$(document).ready(function () {
    var $table = $('#typical-positions-table').DataTable({
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        aoColumnDefs: [
            { "bSearchable": false, "bVisible": true, "aTargets": [ 4 ] }
        ],
        buttons: [
            {
                extend: 'excel',
                title: 'Typical positions',
                exportOptions: {
                    columns: [0, 1, 2, 3]
                }
            },
            {
                extend: 'csv',
                title: 'Typical positions',
                exportOptions: {
                    columns: [0, 1, 2, 3]
                }
            }
        ]
    });
});
