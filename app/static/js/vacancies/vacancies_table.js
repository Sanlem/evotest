/**
 * Created by sanlem on 27.03.17.
 */
$(document).ready(function () {
    var $table = $('#vacancies-table').DataTable({
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        aoColumnDefs: [
            { "bSearchable": false, "bVisible": true, "aTargets": [ 7 ] }
        ],
        buttons: [
            {
                extend: 'excel',
                title: 'Vacancies',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6]
                }
            },
            {
                extend: 'csv',
                title: 'Vacancies',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6]
                }
            }
        ]
    });
});
